document.addEventListener('postchange', function (event) {
  console.log('postchange event', event);
});
function changeTab() {
  document.getElementById('tabbar').setActiveTab(1);
}
function changeButton() {
  document.getElementById('segment').setActiveButton(1);
}
function logIndexes() {
  console.log('active button index', document.getElementById('segment').getActiveButtonIndex());
  console.log('active tab index', document.getElementById('tabbar').getActiveTabIndex());
}
